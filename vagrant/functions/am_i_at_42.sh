#!/bin/bash

function am_i_at_42
{
	HOSTNAME="$(hostname)"
	if [[ $HOSTNAME =~ ^'e'[0-9]r[0-9]+'p'[0-9]+'.42.fr'$ ]]; then
		echo "yes"
	else
		echo "no"
	fi
}
