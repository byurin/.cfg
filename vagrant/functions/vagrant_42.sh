#!/bin/bash

MACHINE_NAME=""
MACHINE_TYPE=""
PROJECT_NAME=""
VAGRANT_SETUP_PATH="~/.cfg/vagrant/setup"

function print_vagrant_42_usage
{
	echo -e "Usage :
	vagrant_42 new_project [PROJECT_NAME]
	vagrant_42 add_machine [PROJECT_NAME] [MACHINE_NAME]"
}

function import_machine_cfg
{
	cat ~/.cfg/vagrant/setup/machines/$MACHINE_TYPE/default_Vagrantfile
}

function get_machine
{
	read -r -p "Do you wish to download this vagrant box : '$MACHINE_TYPE' ? [y/n] " answer
	case "$answer" in
		[yY])
		echo "Downloading..."
		;;
	*)
		exit
		;;
	esac
}

function is_add_machine
{
	if [ "$1" == "add_machine" ] && [ "$#" -eq 3 ]; then
		PROJECT_NAME="$2"
		true
	else
		false
	fi
}

function is_new_project
{
	if [ "$1" == "new_project" ] && [ "$#" -eq 2 ]; then
		PROJECT_NAME="$2"
		true
	else
		false
	fi
}

function exit_perror
{
	if [ "$#" == 1 ]; then
		echo $1
	fi
	exit 1
}

function prompt_remove
{
	read -r -p "Are you sure you want to remove $1 ? [y/n] " answer
	if [[ $answer =~ [yY] ]]; then
		if [ ! $(rm -rvi $1) ]; then
			echo "File wasn't removed, script will exit."
			exit
		fi
	fi
}

function prompt_override
{
	read -r -p "$1 already exists, override ? [yes/no] " answer
	if [ $answer == "yes" ]; then
		prompt_remove $1
		return
	fi
	exit
}

if is_add_machine "$@"; then
	if [ -e $VAGRANT_DIR/$PROJECT_NAME ]; then
		read -r -p "Please input desired machine type (ex: debian/stretch64) : " MACHINE_TYPE
		if [ -e $VAGRANT_SETUP_PATH/machines/$MACHINE_TYPE/ ]; then
			import_machine_cfg
		else
			echo "No default local template for provided machine type."
			get_machine
		fi
	fi
elif is_new_project "$@"; then
	if [ -e $VAGRANT_DIR/$PROJECT_NAME ]; then
		prompt_override $VAGRANT_DIR/$PROJECT_NAME
	fi
	mkdir -p $VAGRANT_DIR/$PROJECT_NAME
	if [ -e $VAGRANT_SETUP_PATH/projects/$PROJECT_NAME/Vagrantfile ]; then
		cp $VAGRANT_SETUP_PATH/projects/$PROJECT_NAME/Vagrantfile $VAGRANT_DIR/$PROJECT_NAME
		# run install
	else
		read -r -p "No project template found, give a Vagrantfile for project ? [y/n] " answer
		case "$answer" in
			[yY])
			read -r -p "Please provide path to desired Vagrantfile : " USER_PROV_VFILE
			echo $USER_PROV_VFILE
			exit
			;;
		esac
		read -r -p "Create an empty project ? [y/n] " answer
		case "$answer" in
			[yY])
			echo "Creating..."
			exit
			;;
			[*])
			exit
			;;
		esac
	fi
else
	print_vagrant_42_usage
	exit 1
fi
