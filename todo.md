TO-DO
=====

## Overall

* Move to github ?
* Cleanup the whole process and scripts organization
* Better compartimentation between OSX/Linux (iterm setup for instance...)
* Uninstall and Update method
* Thanks list with people who helped/inspired this config
* Borrow some functions from jessfraz

## To add

* Better use of zsh settings file (zshenv, zlogin etc...)
