# .cfg
##### WORK IN PROGRESS

Requires git to be installed
  
Heavily borrowed from [Stratus3D dotfiles](https://github.com/Stratus3D/dotfiles)  
With some settings and functions borrowed from [rbobillo](https://github.com/rbobillo)  

Install AT YOUR OWN RISK with :
```curl -s https://bitbucket.org/byurin/.cfg/raw/master/scripts/setup.sh | bash 2>&1 | tee ~/setup.log```
