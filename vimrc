" Your own configurations should go there
if filereadable("~/.usr_cfg/myvimrc")
	so ~/.usr_cfg/myvimrc
endif

set nocompatible

filetype plugin on

set omnifunc=syntaxcomplete#Complete

set guifont=Menlo\ for\ Powerline

"Set help to open in new tab
cabbrev h tab h

""""""""""""
" SUBLIVIM "
"          "
""""""""""""
" Variable configuration for Sublivim
let sbv_theme='onedark'
" Open NERDTree with vim
let sbv_open_nerdtree_to_start=1
" Open Nerd Panel with a new tab
let sbv_open_nerdtree_with_new_tab=1
" Enabled / Disabled placeholder chars
let sbv_display_placeholder=1
" Charactere placeholder for tabulation [2 char]
let sbv_tab_placeholder='»·'
" Charactere placeholder for space [1 char]
let sbv_space_placeholder='·'
" Enabled / Disabled space space for access to vimrc
let sbv_quick_access_config=1
" Enabled / Disabled swap file
let sbv_swap_file=0
" Enabled / Disabled Shortcut
let sbv_smart_shortcut=1
" Indentation type [tab || space]
let sbv_indentation_type="tab"
" Indentation length
let sbv_indentation_length=4
" Relative numbers
let sbv_enable_numbers=1
" Unknown
let current_compiler = "gcc"
let g:enable_numbers = sbv_enable_numbers

execute "set tabstop=". sbv_indentation_length
execute "set shiftwidth=". sbv_indentation_length
execute "set softtabstop=". sbv_indentation_length

if !empty(sbv_smart_shortcut)
	" Sublivim Remap ':q' to C-W
	noremap <S-Tab>				:tabprevious<CR>
	noremap <Tab>				:tabnext<CR>
	noremap <S-Right>			<C-w><Right>
	noremap <S-Left>			<C-w><Left>
	noremap <S-Up>				<C-w><Up>
	noremap <S-Down>			<C-w><Down>
	noremap _		:vertical resize +1<CR>
	noremap -		:vertical resize -1<CR>
	vnoremap <Tab>				>
	vnoremap <S-Tab>			<
endif

"""""""""""""
" SYNTASTIC "
"           "
"""""""""""""
" Syntax checking Syntastic plugin
let g:syntastic_c_compiler = 'gcc'
let g:syntastic_c_compiler_options = ' -std=gnu99 -stdlib=libc++ -Wall -Werror -Wextra'
let g:syntastic_check_on_open= 1
let g:syntastic_enable_signs= 1
let g:syntastic_c_check_header = 1
let g:syntastic_c_remove_include_errors = 1
let g:syntastic_c_auto_refresh_includes = 1
let g:syntastic_c_include_dirs = ['../../../includes', '../../includes','../includes','./includes','../libft/includes']

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"""""""""
" GUNDO "
"       "
"""""""""
" Undo Tree (Gundo) plugin
let g:gundo_width = 60
let g:gundo_preview_height = 40
let g:gundo_right = 1

noremap <C-,> :GundoToggle<CR>
noremap <C-k>				:Silent !make >> output.txt<CR>
noremap <C-g>				:NERDTreeToggle<CR>
inoremap <C-k>				<Esc>:help key-notation<CR>
"inoremap <C-v>				<Esc>pi // Interfers with escape sentence
inoremap <C-c>				<Esc>yi
inoremap <C-x>				<Esc>xi
inoremap <C-u>				<Esc><C-r>i
inoremap <C-a>				<Esc>gg<S-v>G
noremap <C-u>				<C-r>
noremap <C-p>				:!osascript -e 'tell application id "com.apple.ScreenSaver.Engine" to launch'<CR>
noremap <C-n>				:!norminette **/*.{c,h}<CR>
"noremap <C-v>				p // Interfers with Visual Block mode
noremap <C-c>				y
noremap <C-x>				x
noremap <C-a>				gg<S-v>G

""""""""""""""""""""
" MULTIPLE_CURSORS "
"                  "
""""""""""""""""""""
" Multiple cursors Sublivim plugin
"let g:multi_cursor_next_key='<C-d>'
"let g:multi_cursor_prev_key='<C-p>'
"let g:multi_cursor_skip_key='<C-l>'
"let g:multi_cursor_quit_key='<Esc>'
"let g:multi_cursor_start_key='<F6>'
" File finder Ctrlp plugin

""""""""""""
" NERDTREE "
"          "
""""""""""""
" Might be used to ignore files that nerdtree can't read/open
"let NERDTreeIgnore = ['']
let g:current_path_for_nerd_init=expand('%:p:h') " Might have an issue

""""""""""""
" PATHOGEN "
"          "
""""""""""""
call pathogen#infect()
call pathogen#helptags()


""""""""""
" NATIVE "
"        "
""""""""""
syntax on
set encoding=utf-8
set mouse=a
set autoindent
set number
set cc=80
set t_Co=256
set scrolloff=10
set cursorline
set whichwrap+=<,>,h,l,[,]
set splitright
set autochdir
set backspace=indent,eol,start
hi CursorLine term=bold cterm=bold guibg=Grey40

" Fix vim silent commands redraw issue
command! -nargs=+ Silent
\	execute 'silent <args>'
\	| redraw!

""""""""""""""""""""""
" SUBLIVIM FUNCTIONS "
"                    "
""""""""""""""""""""""
if sbv_indentation_type == "space"
	set expandtab
endif

if !empty(sbv_quick_access_config)
	noremap <Space><Space>		:tabedit ~/.vimrc<CR>
endif

if !empty(sbv_theme)
	execute "colorscheme ".  sbv_theme
endif

if empty(sbv_swap_file)
	set noswapfile
endif

if !empty(sbv_display_placeholder)
	execute "set list listchars=tab:". sbv_tab_placeholder .",trail:". sbv_space_placeholder
endif

" Opens nerdtree at startup
autocmd VimEnter * call s:actionForOpen(sbv_open_nerdtree_to_start)
function! s:actionForOpen(openNerdTree)
	let filename = expand('%:t')
	if !empty(a:openNerdTree)
		NERDTree
	endif
	if !empty(filename)
		wincmd l
	endif
endfunction

" Open nerdtree with any new tab
autocmd BufCreate * call s:addingNewTab(sbv_open_nerdtree_with_new_tab)
function! s:addingNewTab(openNerdTree)
	let filename = expand('%:t')
	if winnr('$') < 2 && exists('t:NERDTreeBufName') == 0
		if !empty(a:openNerdTree)
			NERDTree
		endif
		if !empty(filename)
			wincmd l
		endif
	endif
endfunction

" Close nerdtree if it's the last remaining pane
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

"""""""""""""""""""
" OTHER FUNCTIONS "
"                 "
"""""""""""""""""""
"Delete spaces at the end of a line
autocmd BufWritePre *.c :%s/\s\+$//e

"Norme
match Error /\(^{\n\)\(^.*\n\(^}\)\@!\)\{25}\zs\(^.*\n\(^\i\)\@!\)*\ze\(^}\)/
match Error /  \+/
