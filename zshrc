export ZSH=$HOME/.oh-my-zsh

ZSH_THEME="agnoster"

plugins=(
	git
)

source $ZSH/oh-my-zsh.sh

# For .cfg repo to work correctly the paths to source'd files must be
# relative to the location of this file. This doesn't handle cases where the
# .zshrc is symlinked to a symlink.
# Taken from http://stackoverflow.com/a/26492107/1245380
ZSHRC_PATH=$(dirname "$(readlink "${(%):-%N}")")

source $ZSHRC_PATH/mixins/functions
source $ZSHRC_PATH/mixins/path
source $ZSHRC_PATH/mixins/aliases

# Source user cfg after in order to let user override any previous settings on
# local machine
# Your own configurations should go there

if [ -f $HOME/.usr_cfg/myzshrc ]; then
	source $HOME/.usr_cfg/myzshrc
fi

if [ -f $HOME/.usr_cfg/mixins/aliases ]; then
	source $HOME/.usr_cfg/mixins/aliases
fi

if [ -f $HOME/.usr_cfg/mixins/path ]; then
	source $HOME/.usr_cfg/mixins/path
fi

if [ -f $HOME/.usr_cfg/mixins/functions ]; then
	source $HOME/.usr_cfg/mixins/functions
fi


# Manage my current project env variable file
if [ ! -f $HOME/.cfg/.curr_prodir.sh ]; then
	if [ -d $DEVELDIR ]; then
		echo "export CUR_PRODIR=$DEVELDIR" > $HOME/.cfg/.curr_prodir.sh;
	else
		touch $HOME/.cfg/.curr_prodir.sh
	fi
fi
source $HOME/.cfg/.curr_prodir.sh

# ------ Sets

mesg n

# Save all history
# Incrementally write history to file
setopt INC_APPEND_HISTORY
# Save timestamp to history file too
setopt EXTENDED_HISTORY
# Import newly written commands from the history file
setopt SHARE_HISTORY

precmd() {
	if [ "$(id -u)" != "0" ]; then
		echo "$(date "+%Y-%m-%d_%H:%M:%S") $(pwd) $(history | tail -n 1)" >>! $HOME/history/zsh_history-$(date "+%Y-%m-%d").log;
	fi
}

if [ -f ~/.myzshrc ]; then
	source ~/.myzshrc
fi

USER=`/usr/bin/whoami`
export USER
GROUP=`/usr/bin/id -gn $user`
export GROUP
MAIL="$USER@student.42.fr"
export MAIL

# ------ Setting Terminal UX

#export PS1="%F{red}[%F{white}%m%F{red}]%F{white}:%F{cyan}%~%F{white}%f$ "
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced
export DEFAULT_USER=`whoami`

# ------ Utility

# Change current project dir env
function chcpd() {
	echo "export CUR_PRODIR=$(pwd)" > $HOME/.cfg/.curr_prodir.sh
	source $HOME/.cfg/.curr_prodir.sh
}

function ccpd() {
	source $HOME/.cfg/.curr_prodir.sh
	cd $CUR_PRODIR
}

# Load Homebrew config script
source $HOME/.brewconfig.zsh

# School vagrant setup
export VAGRANT_DIR=~/goinfre/vagrant
export VAGRANT_HOME=$VAGRANT_DIR/setup
