#!/bin/bash -
###############################################################################
# setup.sh
# New environment setup script
###############################################################################

# Unoffical Bash "strict mode"
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\t\n' # Stricter IFS settings
ORIGINAL_IFS=$IFS

cd $HOME

CFG_DIR=$HOME/.cfg
CFG_SCRIPTS_DIR=$CFG_DIR/scripts

# Get OS type
if [[ "$(uname)" == 'Darwin' ]]; then
	OS_SPECIFIC_SETUP="$CFG_SCRIPTS_DIR/setup/darwin.sh" #========================================== 1st switch to darwin.sh
elif [[ "$(uname)" == 'Linux' ]]; then
	OS_SPECIFIC_SETUP="$CFG_SCRIPTS_DIR/setup/linux.sh"
else
	echo "Error : Unsupported OS type '$(uname)'"
	exit 1
fi

###############################################################################
# Setup dotfiles
###############################################################################

if [ ! -d $CFG_DIR ]; then
	echo "Cloning repository..."
	git clone https://bitbucket.org/byurin/.cfg
else
	cd $CFG_DIR
	# If there are modifications in the repository, stash them
	if [[ `git status --porcelain` != "" ]]; then
		git stash save -u
		git pull origin master
		git stash pop
	fi
fi

. $HOME/.cfg/scripts/setup/setup_functions/var_display
. $HOME/.cfg/scripts/setup/setup_functions/utils

cd $CFG_DIR

###############################################################################
# Create commonly used directories
###############################################################################

print_title "Creating commonly used directories"

mkdir -p $HOME/bin # Third-party binaries
mkdir -p $HOME/lib # Third-party software
mkdir -p $HOME/history # Zsh and Bash history files ============================== Look at how history files are put there
mkdir -p $HOME/.usr_cfg
if [[ "$(uname)" == 'Darwin' ]]; then
	mkdir -p $HOME/.usr_cfg/iterm2_profile
fi
mkdir -p $HOME/Devel
#mkdir -p $HOME/Devel/src # Source directory
#mkdir -p $HOME/Devel/bin # Binary directory
#mkdir -p $HOME/Devel/archived # Old projects

###############################################################################
# Create user defined configuration files
###############################################################################

print_title "Creating user configuration files"

touch $HOME/.usr_cfg/myvimrc
touch $HOME/.usr_cfg/myzshrc
touch $HOME/.usr_cfg/aliases

echo "\" User defined vim configurations should go there" >> $HOME/.usr_cfg/myvimrc
echo "# User defined zsh configurations should go there" >> $HOME/.usr_cfg/myzshrc
echo "# User defined aliases should go there" >> $HOME/.usr_cfg/aliases

###############################################################################
# Install software
###############################################################################

print_title "Installing software"

# Get the uname string
unamestr=`uname`

# Install oh-my-zsh first, as the laptop script doesn't install it
ZSH_DIR="$HOME/.oh-my-zsh" #====================================================== Ohmyzsh install
if [[ -d $ZSH_DIR ]]; then
	# Update Zsh if we already have it installed
	cd $ZSH_DIR
	git pull origin master
	cd -
else
	# Install it if don't have a ~/.oh-my-zsh directory
	curl -L http://install.ohmyz.sh | sh
fi

# Run the OS-specific setup scripts
$OS_SPECIFIC_SETUP

###############################################################################
# Create symlinks to custom config now that all the software is installed
###############################################################################

print_title "Symlinking"

$CFG_SCRIPTS_DIR/makesymlinks.sh #================================================ 2nd switch to makesymlinks.sh

if [[ "$(uname)" == 'Darwin' ]]; then
	# Setup iterm preferences and import custom font
	$CFG_DIR/iterm2_profile/iterm_setup.sh
	echo "===> Please restart iterm entirely for changes to take effect... <==="
else
	echo "Installation succesful !"
fi
