#!/bin/bash -
###############################################################################
# Set flags so script is executed in "strict mode"
# Quite a dirty script here
###############################################################################

# Unoffical Bash "strict mode"
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -eo pipefail
IFS=$'\t\n' # Stricter IFS settings
ORIGINAL_IFS=$IFS

wd_git_url="$(git config --get remote.origin.url)"

###############################################################################
# For each repo's url received, check wether a corresponding repo exists in wd
# If it doesn't then clone the repo
# If it does updates it
###############################################################################

git_install_or_update() {
flag=0
for dir in */; do
	if [ $dir == "$(basename $1)/" ]; then
		flag=1
		cd $dir
		if [ -d .git ]; then
			`git remote update > /dev/null` # Update remote info
			if [[ `git status -s -uno` != "" ]]; then
				git pull origin master
				printf "\n"
			else
				printf "%-30s OK" $dir
			fi
			printf "\n"
		fi
		cd - > /dev/null
	fi
done
if [ "$flag" -eq 0 ]; then
	echo "Test $1"
	git clone $1
fi
}

###############################################################################

if [ "$1" == "" ]; then
	printf "usage: \e[91mgitins \e[92m[Chosen path] \e[92m[Opt list of repos to install, if unset just update repos in path] \e[0m\n"
	exit
fi

if [ -d "$1" ]; then
	cd $1
	if [ "$2" == "" ]; then
		for dir in */; do # For every subdir in working dir
			cd $dir
			if [ -d .git ]; then # If subdir is a repo
				`git remote update > /dev/null` # Update remote info
				if [[ `git status -s -uno` != "" ]]; then # If not up to date
					printf $dir
					git pull origin master # Then update repo
					printf "\n"
				else
					printf "%-30s OK" $dir
				fi
				printf "\n"
			fi
			cd - > /dev/null
		done
	elif [ "$2" != "" ]; then # If there's a 2nd argument (URL list)
		filelines=`cat $2`
		for url in $filelines; do
			git_install_or_update $url
		done
	fi
fi


