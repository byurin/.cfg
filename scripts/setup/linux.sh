#!/usr/bin/env bash
###############################################################################
# Set flags so script is executed in "strict mode"
###############################################################################

# Unoffical Bash "strict mode"
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\t\n' # Stricter IFS settings
ORIGINAL_IFS=$IFS

# Get the directory this script is stored in
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

. $HOME/.cfg/scripts/setup/setup_functions/var_display
. $HOME/.cfg/scripts/setup/setup_functions/utils

###############################################################################
# Run thoughtbot's laptop script and install missing packages with brew
###############################################################################

# Welcome to the thoughtbot laptop script!
# Be prepared to turn your laptop (or desktop, no haters here)
# into an awesome development machine.

trap 'ret=$?; test $ret -ne 0 && printf "failed\n\n" >&2; exit $ret' EXIT

set -e

if [ ! -f "$HOME/.zshrc" ]; then
	touch "$HOME/.zshrc"
fi

case "$SHELL" in
	*/zsh) : ;;
	*)
		fancy_echo "Changing your shell to zsh ..."
			chsh -s "$(which zsh)"
	;;
esac

# TODO : determine pacman or apt-get
# set packages outside of darwin.sh and linux.sh
#fancy_echo "Updating Homebrew formulas ..."
#brew update

#brew_install_or_upgrade 'zsh'
#brew_install_or_upgrade 'git'
#brew_install_or_upgrade 'vim'
#brew_install_or_upgrade 'ctags'
#brew_install_or_upgrade 'zsh-completions'

#brew_install_or_upgrade 'the_silver_searcher'
#brew_install_or_upgrade 'tmux'
#brew_install_or_upgrade 'reattach-to-user-namespace'
#brew_install_or_upgrade 'imagemagick'
#brew_install_or_upgrade 'qt'
#brew_install_or_upgrade 'hub'
#brew_install_or_upgrade 'shellcheck'

#brew_install_or_upgrade 'openssl'
#brew unlink openssl && brew link openssl --force
#brew_install_or_upgrade 'libyaml'

#brew_install_or_upgrade 'perl'
#brew_install_or_upgrade 'python'
#brew_install_or_upgrade 'python3'


###############################################################################
# End of thoughtbot's laptop script
###############################################################################

# Exuberant Ctags
#brew install ctags

# Visualization library <======================================================== Might be useful to visualize my algos !
#brew install graphviz
#brew link graphviz

# Install command-line JSON processor
#brew install jq


# Install pianobar for music
#brew install pianobar

# QCacheGrind for valgrind analysis
#brew install qcachegrind --with-graphviz
#brew linkapps qcachegrind

# Install GNU readlink
#brew install coreutils

# Install GNU sed
#brew install gnu-sed

# autoexpect
#brew install expect

# For adb
#brew install android-platform-tools

# Mosh for high latency remote servers
#brew install mobile-shell

# Yarn for package management
#brew install yarn

# Elm for packages that require it
#brew install elm

# For network troubleshooting
#brew install mtr

# OSX alternative to `ps auxf` for process tree views
#brew install pstree

# Images in the terminal
#brew tap eddieantonio/eddieantonio
#brew install imgcat

# Install tools for server testing and administration
#brew install ansible
#brew cask install vagrant
#brew cask install vagrant-manager # OSX toolbar for vagrant

# Install iperf3 for network performance tests
#brew install iperf3

# Install other software using custom install scripts
#install_scripts=(
#    # Testing
#    bats.sh
#    # JavaScript
#    doctorjs.sh
#    # Ruby
#    prax.sh
#    # Erlang
#    erlgrind.sh
#    observer_cli.sh
#    rebar.sh
#    rebar3.sh
#    recon.sh
#    relx.sh
#    sync.sh
#    # OSX only
#    osxfuse.sh
#    zeal.sh
#)

#run_install_scripts $install_scripts
