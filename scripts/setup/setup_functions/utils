append_to_zshrc() {
	local text="$1" zshrc
	local skip_new_line="${2:-0}"

	if [ -w "$HOME/.zshrc.local" ]; then
		zshrc="$HOME/.zshrc.local"
	else
		zshrc="$HOME/.zshrc"
	fi

	if ! grep -Fqs "$text" "$zshrc"; then
		if [ "$skip_new_line" -eq 1 ]; then
			printf "%s\n" "$text" >> "$zshrc"
		else
			printf "\n%s\n" "$text" >> "$zshrc"
		fi
	fi
}

run_install_scripts() {
	scripts_list="$1"
	install_scripts_dir=$HOME/.cfg/scripts/setup/install

	# Run each script
	for file in $scripts_list; do
		"$install_scripts_dir/$file"
	done
}

brew_install_or_upgrade() {
	if brew_is_installed "$1"; then
		if brew_is_upgradable "$1"; then
			fancy_echo "Upgrading %s ..." "$1"
			brew upgrade "$@"
		else
			fancy_echo "Already using the latest version of %s. Skipping ..." "$1"
		fi
	else
		fancy_echo "Installing %s ..." "$1"
		brew install "$@"
	fi
}

brew_is_installed() {
	local name="$(brew_expand_alias "$1")"

	brew list -1 | grep -Fqx "$name"
}

brew_is_upgradable() {
	local name="$(brew_expand_alias "$1")"

	! brew outdated --quiet "$name" >/dev/null
}

brew_tap() {
	brew tap "$1" 2> /dev/null
}

brew_expand_alias() {
	brew info "$1" 2>/dev/null | head -1 | awk '{gsub(/:/, ""); print $1}'
}
