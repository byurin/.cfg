#!/bin/bash

# Placeholder for various functions originally contained in zshrc
# (now sourced as follow : "zshrc -> mixins/functions -> scripts/functions/*")

###############################################################################
# Cool options
# TODO : Tidy up this file
###############################################################################

# Change current shell title
title(){
	print -Pn "\e]2; $1 \a"
}

# Oneliner to extract function prototypes for *.c files in current directory
function extract_cfp
{
	cat *.c | grep -E "^\w" | sed 's/$/;/'
}

# Find occurence of a string in all subdirectories
function grepf
{
	if [ "$1" = "" ] ; then
		printf "usage: \e[91mgf \e[92m<string_to_look_for>\e[0m\n"
	else
		for file in `find . -type f` ; do
			cat $file | grep $1 > /dev/null &&
				(printf "\e[92m $file\e[0m\n" &&
				cat -n $file | grep $1 | awk '{printf " line", gsub(/[ |\t]+/," ")}1') ||
				printf "\e[91m$file\e[0m\n"
		done
	fi
}

# Find all .c files in subdirectories and list some of their attributes
function fnum
{
	if [ "$1" = "" ]
	then
		for files in `find . -iname '*.c'`
		do
			printf "\033[4;96m" ; print "$files" | awk '{print toupper($0)}'; printf "\033[0m"
			cat $files | grep -w "int\|char\|void\|size_t\|t_size\|t_data\|t_set\|short\|long\|double\|float\|extern\|const\|static" | grep -v ";" | grep "(" | grep -v "[	| ]if (\|[	| ]while (" | grep -v "=" | sed 's/(.*/()/g' | awk '{printf "     \033[32m", gsub(/[	]+/,"	\033[0m")}1';
			echo "";
		done
	else
		printf "\033[4;96m" ; print "$1" | awk '{print toupper($0)}'; printf "\033[0m"
		cat $1 | grep -w "int\|char\|void\|size_t\|t_size\|t_data\|t_set\|short\|long\|double\|float\|extern\|const\|static" | grep -v ";" | grep "(" | grep -v "[	| ]if (\|[	| ]while (" | grep -v "=" | sed 's/(.*/()/g' | awk '{printf "     \033[32m", gsub(/[	]+/,"	\033[0m")}1';
		echo "";
	fi
}

# Git add, commit and push every modified files
function cgit
{
	for i in `git status | grep -i modified | awk '{print $2}'` ; do git add $i ; done &&
		git commit -m "$(date)" &&
			git push origin master
}

# Get local ip
function myip
{
	ifconfig | grep "inet " | grep -v 127.0.0.1 | awk '{print $2}';
}

# Easy makefiler
makefile(){
	if [ "$1" = "" ]
	then
		clear
		printf "\n\e[1;4mUtilisation de MAKEFILER:\e[0m\n\n"
		printf "\e[91mmakefile\e[0m \e[92m<NAME>\e[0m\n\n"
		printf "<NAME> -> Nom de l'executable final\n\n"
	else
		echo "NAME\t=\t$1\n\nCC\t\t=\tgcc\n\nSRC\t\t=\t\c" > Makefile ;
		ls -m *.c | tr -d '\n' | sed 's/, / \\\
			/g' >> Makefile ;
		echo "\nOBJ\t\t=\t\$(SRC:.c=.o)\n\nLIB\t\t=\tlibft/libft.a\n\nFLAG\t=\t-Wall -Wextra -Werror\n\n\nall\t\t:\t\$(NAME)\n\n\$(NAME)\t:\t\$(OBJ)\n\t\t\tgcc \$(FLAG) -o \$(NAME) \$(SRC) \$(LIB)\n\nclean\t:\n\t\t\trm -f \$(OBJ)\n\nfclean\t:\tclean\n\t\t\trm -f \$(NAME)\n\nre\t\t:\tfclean all" >> Makefile ;
	fi
}

# Easy makefilerpp
makefilepp(){
	if [ "$1" = "" ]
	then
		clear
		printf "\n\e[1;4mUtilisation de MAKEFILER:\e[0m\n\n"
		printf "\e[91mmakefile\e[0m \e[92m<NAME>\e[0m\n\n"
		printf "<NAME> -> Nom de l'executable final\n\n"
	else
		echo "NAME\t=\t$1\n\nCC\t\t=\tg++\n\nSRC\t\t=\t\c" > Makefile ;
		ls -m *.cpp | tr -d '\n' | sed 's/, / \\\
		/g' >> Makefile ;
		echo "\nOBJ\t\t=\t\$(SRC:.cpp=.o)\n\nFLAG\t=\t-Wall -Wextra -Werror\n\n\nall\t\t:\t\$(NAME)\n\n\$(NAME)\t:\t\$(OBJ)\n\t\t\t\$(CC) \$(FLAG) -o \$(NAME) \$(SRC) \$(LIB)\n\nclean\t:\n\t\t\trm -f \$(OBJ)\n\nfclean\t:\tclean\n\t\t\trm -f \$(NAME)\n\nre\t\t:\tfclean all" >> Makefile ;
	fi
}

# Easy makelib
makelib(){
	echo "NAME\t=\tlibft.a\n\nCC\t\t=\tgcc\n\nSRC\t\t=\t\c" > Makefile ;
	ls -m ft_*.c | tr -d '\n' | sed 's/, / \\\
	/g' >> Makefile ;
	echo "\nOBJ\t\t=\t\$(SRC:.c=.o)\n\nFLAG\t=\t-Wall -Wextra -Werror\n\nall\t\t:\t\$(NAME)\n\n\$(NAME)\t:\n\t\t\t@echo \"Compiling the \\033[96mlibft\\033[0m\"\n\t\t\t@gcc \$(FLAG) -c \$(SRC)\n\t\t\t@ar rc \$(NAME) \$(OBJ)\n\t\t\t@ranlib \$(NAME)\n\t\t\t@echo \"\\033[92mDone\\033[0m\"\n\nclean\t:\n\t\t\t@echo \"Cleaning the \033[96mlibft\033[0m files\"\n\t\t\t@rm -f \$(OBJ)\n\t\t\t@echo \"\\033[92mDone\\033[0m\"\n\nfclean\t:\tclean\n\t\t\t@rm -f \$(NAME)\n\nre\t\t:\tfclean all" >> Makefile ;
}

timer()
{
	for h in {00..23} ; do for m in {00..59} ; do for s in {00..59} ; do printf "\e[93m$h\e[0m:\e[92m$m\e[0m:\e[91m$s\e[0m\r"; sleep 1 ; done ; done ; done
}

# Set mac volume to 0
v0()
{
	ssh -T $1 -f "osascript -e 'set volume 0' ; exit"
}

# Set desired mac volume
vol(){
	osascript -e "set volume $1";
}

# Display dialog box
dialog(){
	osascript -e "tell app \"System Events\" to display dialog \"$1\"" > /dev/null
}

# Display notification box
notif(){
	osascript -e "tell app \"System Events\" to display notification \"$1\"" > /dev/null
}
